// var btn=document.querySelector('#twofuns');
// btn.addEventListener('click', refpage);
// btn.addEventListener('click', createUsTable);

// function refpage(){
//     window.location.reload();
// };


function createUsTable(){
    const data=fetch("https://gitlab.com/zmey2k/appsdynamics/-/raw/main/usAll.json").then(function(resp){
        return resp.json();
    }).then(function(data){
        buildTableUS(data);
    });
    function buildTableUS(d){
        let table=document.getElementById('buildTable');
        for(let i=0; i<d.length; i++){
            let row=`<tr>
                        <td>${d[i].data_date}</td>
                        <td>${d[i].estimated_ecpm}</td>
                    </tr>`
            table.innerHTML+=row
        };
    };
}


function createUkTable(){
    const data1=fetch("./ukAll.json").then(function(resp1){
        return resp1.json();
    }).then(function(data1){
        buildTableUk(data1);
    });
    function buildTableUk(d){
        let table1 =document.getElementById('buildTable');
        for(let i=0; i<d.length; i++){
            let row1=`<tr>
                        <td>${d[i].data_date}</td>
                        <td>${d[i].estimated_ecpm}</td>
                    </tr>`
            table1.innerHTML+=row1
        }
    }
}
